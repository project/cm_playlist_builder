<?php
/**
 * @file
 * cm_playlist_builder.features.inc
 */

/**
 * Implements hook_views_api().
 */
function cm_playlist_builder_views_api() {
  return array("version" => "3.0");
}
