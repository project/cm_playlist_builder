<?php
/**
 * @file
 * cm_playlist_builder.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cm_playlist_builder_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'interstitial_playlists';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Interstitial Playlists';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'cm_playlist' => 'cm_playlist',
  );
  /* Filter criterion: Content: This is an Interstitial Playlist (field_interstitial) */
  $handler->display->display_options['filters']['field_interstitial_value']['id'] = 'field_interstitial_value';
  $handler->display->display_options['filters']['field_interstitial_value']['table'] = 'field_data_field_interstitial';
  $handler->display->display_options['filters']['field_interstitial_value']['field'] = 'field_interstitial_value';
  $handler->display->display_options['filters']['field_interstitial_value']['value'] = array(
    1 => '1',
  );

  /* Display: References */
  $handler = $view->new_display('references', 'References', 'references_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'references_style';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'references_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $translatables['interstitial_playlists'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('References'),
  );
  $export['interstitial_playlists'] = $view;

  $view = new view();
  $view->name = 'playlists';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Playlists';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Playlists';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<ul class="action-links"><li><a href="/node/add/cm-playlist?destination=admin/content/playlists">Add Playlist</a></li></ul>';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = 'Duration';
  /* Field: Content: Interstitial Playlist */
  $handler->display->display_options['fields']['field_interstitial_playlist']['id'] = 'field_interstitial_playlist';
  $handler->display->display_options['fields']['field_interstitial_playlist']['table'] = 'field_data_field_interstitial_playlist';
  $handler->display->display_options['fields']['field_interstitial_playlist']['field'] = 'field_interstitial_playlist';
  $handler->display->display_options['fields']['field_interstitial_playlist']['label'] = 'Related Interstitial Playlist';
  $handler->display->display_options['fields']['field_interstitial_playlist']['element_type'] = '0';
  $handler->display->display_options['fields']['field_interstitial_playlist']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_interstitial_playlist']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_interstitial_playlist']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_interstitial_playlist']['field_api_classes'] = TRUE;
  /* Field: Content: queue_ref */
  $handler->display->display_options['fields']['field_queue_ref']['id'] = 'field_queue_ref';
  $handler->display->display_options['fields']['field_queue_ref']['table'] = 'field_data_field_queue_ref';
  $handler->display->display_options['fields']['field_queue_ref']['field'] = 'field_queue_ref';
  $handler->display->display_options['fields']['field_queue_ref']['label'] = 'Edit';
  $handler->display->display_options['fields']['field_queue_ref']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_queue_ref']['alter']['text'] = '<a href="/admin/structure/nodequeue/[field_queue_ref]/view">Edit</a>';
  $handler->display->display_options['fields']['field_queue_ref']['alter']['path'] = 'admin/structure/nodequeue/[field_queue_ref-value]/view/[field_queue_ref-value]';
  $handler->display->display_options['fields']['field_queue_ref']['alter']['alt'] = 'Edit';
  $handler->display->display_options['fields']['field_queue_ref']['element_type'] = '0';
  $handler->display->display_options['fields']['field_queue_ref']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_queue_ref']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_queue_ref']['type'] = 'text_plain';
  /* Field: Content: This is an Interstitial Playlist */
  $handler->display->display_options['fields']['field_interstitial']['id'] = 'field_interstitial';
  $handler->display->display_options['fields']['field_interstitial']['table'] = 'field_data_field_interstitial';
  $handler->display->display_options['fields']['field_interstitial']['field'] = 'field_interstitial';
  $handler->display->display_options['fields']['field_interstitial']['label'] = 'Interstitial';
  $handler->display->display_options['fields']['field_interstitial']['element_type'] = '0';
  $handler->display->display_options['fields']['field_interstitial']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_interstitial']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_interstitial']['field_api_classes'] = TRUE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'cm_playlist' => 'cm_playlist',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'admin/content/playlists';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Playlists';
  $handler->display->display_options['menu']['weight'] = '99';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $translatables['playlists'] = array(
    t('Master'),
    t('Playlists'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('<ul class="action-links"><li><a href="/node/add/cm-playlist?destination=admin/content/playlists">Add Playlist</a></li></ul>'),
    t('Title'),
    t('Duration'),
    t('Related Interstitial Playlist'),
    t('Edit'),
    t('<a href="/admin/structure/nodequeue/[field_queue_ref]/view">Edit</a>'),
    t('Interstitial'),
    t('Page'),
  );
  $export['playlists'] = $view;

  return $export;
}
